# yocto-manifests
Yocto manifests for experiments on various hardware platforms

Usage:

```bash
$ curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
$ chmod a+x ~/bin/repo
$ mkdir working-directory-name 
$ cd working-directory-name 
$ repo init -u https://MaxPayne86@bitbucket.org/MaxPayne86/yocto-manifests-public.git -b master -m select_your_manifest.xml
$ repo sync
```
